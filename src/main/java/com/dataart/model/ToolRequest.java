package com.dataart.model;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * Created by ntkachev on 03/22/17.
 */
public class ToolRequest {
    private User requester;
    private User assignee;
    private Tool tool;
    private LocalDateTime creationDate;
    private LocalDateTime validUntil;
    private Status status;

    private enum Status {
        OPEN,
        OWNER_ACCEPTED,
        OWNER_PASSED,
        REQUESTER_RECEIVED,
        REQUESTER_RETURNED,
        OWNER_RECEIVED_BACK,
        CLOSED,
        DECLINED
    }
}
