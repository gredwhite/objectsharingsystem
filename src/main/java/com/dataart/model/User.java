package com.dataart.model;

import java.util.Set;

/**
 * Created by ntkachev on 03/22/17.
 */
public class User {
    private String name;
    private Set<Tool> tools;
    private Set<ToolRequest> toolRequests;
}
