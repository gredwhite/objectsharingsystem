package com.dataart.model;

import java.util.Set;

/**
 * Created by ntkachev on 03/22/17.
 */
public class Tool {
    private User owner;
    private String name;
    private String pathToImage;
    private String description;
    private Set<ToolRequest> toolRequests;
}
