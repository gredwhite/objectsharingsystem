package com.dataart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ObjectSharingSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(ObjectSharingSystemApplication.class, args);
    }
}
